AppInit = {};
globals = {};

Ti.include(
	'lib/database.js', 
	'lib/login.js', 
	'lib/styles.js', 
	'lib/utilities.js'
);
require('lib/requirePatch').monkeypatch(this);

globals.ai = Utils.activityIndicator();
globals.logged = false;
globals.SITE_URL = 'http://www.stuffsafe.com/';
globals.AUTH_KEY;
globals.USER_TYPE;
globals.UPDATE_FORM_INIT = false;
globals.rooms = [];
globals.roomID;
globals.remove = {
	itemType: '',
	itemID: ''
};
Database.createDatabase();

/*
 * Login window
 */
AppInit.loginWindow = Ti.UI.createWindow({
	title: 'Login',
	backgroundColor: '#eeefe1'
});

Login.tryAutoLogin();

AppInit.spinUpApp = function() {
	Ti.include(
		'lib/properties.js',
		'lib/inventory.js',
		'lib/reports.js',
		'ui/DashboardWindow.js',
		'ui/PropertyWindow.js',
		'ui/InventoryWindow.js',
		'ui/InventoryModalWindow.js',
		'ui/MapModalWindow.js',
		'ui/InventoryDetailView.js',
		'ui/DocumentView.js',
		'ui/InventoryUpdateView.js'
	);
	globals.moment = require('lib/moment');
	Dashboard.init();
	Inventory.getCategories();
	
	Reports.getRptInventoryByCategory();
	Reports.getRptPurchaseReplacementByCategory();
	
	/*
	 * Get properties
	 */
	Properties.getProperties();
	
	AppInit.masterWindow = Ti.UI.createWindow({
		barColor: '#94b735',
		title: 'Properties',
		backgroundColor: '#eeefe1'
	});
	AppInit.detailWindow = Ti.UI.createWindow({
		barColor: '#94b735',
		title: 'StuffSafe',
		backgroundColor: '#eeefe1'
	});	
	
	/*
	 * Master/Detail nav windows and splitView
	 */
	AppInit.masterNav = Ti.UI.iPhone.createNavigationGroup({
		window: AppInit.masterWindow
	});
	AppInit.detailNav = Ti.UI.iPhone.createNavigationGroup({
		window: AppInit.detailWindow
	});	
	AppInit.splitView = Titanium.UI.iPad.createSplitWindow({
		masterView: AppInit.masterNav,
		detailView: AppInit.detailNav
	});
	
	AppInit.splitView.addEventListener('visible',function(e) {
		if (e.view == 'detail')	{
			e.button.title = 'Menu';
			AppInit.detailWindow.leftNavButton = e.button;
		} else if (e.view == 'master') {
			AppInit.detailWindow.leftNavButton = null;
		}
	});
	
	AppInit.mainMenuView = Ti.UI.createTableView();
	
	AppInit.mainMenuView.addEventListener('click', function(e) {	
		AppInit.showRoomView(e);
	});
	
	AppInit.showRoomView = function(e) {
		Property.init(e.rowData.propertyID);
		AppInit.detailWindow.title = e.rowData.propertyName;
		globals.propertyID = e.rowData.propertyID;
				
		var roomMenuWindow = Ti.UI.createWindow({
			barColor: '#94b735',
			backgroundColor: '#eeefe1'
		});
		var roomMenuView = Ti.UI.createTableView();
		
		var rData = [], row, lTitle, lCount, rObj;
		
		globals.rooms = e.rowData.rooms;
		for (var rIndex = 0; rIndex < (e.rowData.rooms.length); rIndex++ ) {
			rObj = e.rowData.rooms[rIndex];
			row = Ti.UI.createTableViewRow({height: 40, hasChild: false, propertyID: e.rowData.propertyID, propertyName: e.rowData.propertyName, roomID: rObj.RoomID, roomName: rObj.RoomName });
			lTitle = Ti.UI.createLabel({text: rObj.RoomName, left: 8, font: {fontSize: 16, fontWeight: 'bold'}});
			lCount = Ti.UI.createLabel(Styles.tableViewBubble(rObj.RoomItemCount));
			row.add(lTitle,lCount);
			rData.push(row);
		}
		roomMenuView.setData(rData);
		roomMenuWindow.add(roomMenuView);
		AppInit.masterNav.open(roomMenuWindow,{animated: true});
		
		roomMenuView.addEventListener('click', function(e) {
			var propertyID = e.rowData.propertyID;
			var roomID     = e.rowData.roomID;
			
			AppInit.detailWindow.title = e.rowData.propertyName+' : '+e.rowData.roomName;
			AppInit.splitView.setMasterPopupVisible(false);
			InventoryView.init(propertyID,roomID);
		});
		
		AppInit.detailWindow.rightNavButton = AppInit.buttonBar;	
	}
	
	AppInit.masterWindow.add(AppInit.mainMenuView);
	AppInit.detailWindow.animate({view: Dashboard.view, transition: Ti.UI.iOS.ANIMATION_CURVE_EASE_IN});
	AppInit.masterWindow.addEventListener('focus', function(e) {
		AppInit.detailWindow.animate({view: Dashboard.view, transition: Ti.UI.iOS.ANIMATION_CURVE_EASE_IN});
		AppInit.detailWindow.rightNavButton = null;	
	});
	
	/*
	 * Add right nav button for adding new item.
	 */
	AppInit.buttonBar = Titanium.UI.createButtonBar({
		labels: [
			{image: 'images/home_16x16.png', width: 50},
			{image: 'images/plus_16x16.png', width: 50}
		], 
		backgroundColor: '#8cc5f2',
		style: Titanium.UI.iPhone.SystemButtonStyle.BAR
	});
	AppInit.buttonBar.addEventListener('click', function(e) {
		var btnIndex = e.index;
		if (btnIndex == 0) {
			AppInit.detailWindow.animate({view: Dashboard.view, transition: Ti.UI.iOS.ANIMATION_CURVE_EASE_IN});
			AppInit.detailWindow.rightNavButton = null;	
			AppInit.masterNav.open(AppInit.masterWindow);
		} else if (btnIndex == 1) {	
			InventoryModalWindow.init(null,'add');
			InventoryModalWindow.win.open({
				modal: true,
				modalStyle: Titanium.UI.iPhone.MODAL_PRESENTATION_FORMSHEET
			});
		}	
	});
	
	AppInit.open = function() {
		AppInit.splitView.open(); 
	};
	
	/*
	 * Add a button to our initial master view window
	 * This is how we would do it...should we choose to.
	 */
	/*
	var b = Titanium.UI.createButton({
		title: 'Button',
		style: Titanium.UI.iPhone.SystemButtonStyle.BORDERED,
		enabled: true
	});
	b.addEventListener('click', function() {
		Ti.API.info('Clicked left button!');
	});
	AppInit.masterWindow.setToolbar([b]);
	*/
	
	AppInit.open();
}










