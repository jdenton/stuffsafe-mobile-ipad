Inventory = {};

Inventory.getInventory = function(propertyID,roomID) {
	globals.ai._show({message: 'Getting inventory...'});
	var ajax = Titanium.Network.createHTTPClient();
	ajax.onerror = function(e) {
		alert('Error');
	};
	ajax.onload = function() {
		var data = this.responseText;
		var jdata = JSON.parse(data);
		Inventory.collection = jdata;
		InventoryView.renderItems();
	};
	ajax.open('POST', globals.SITE_URL+'/inventory/getInventoryForRoom/'+roomID+'/json/0');
	ajax.send({
		'authKey'  : globals.AUTH_KEY
	});
}	

Inventory.getInventoryItem = function(inventoryID,action) {
	globals.ai._show({message: 'Getting inventory...'});
	var ajax = Titanium.Network.createHTTPClient();
	ajax.onerror = function(e) {
		alert('Error');
	};
	ajax.onload = function() {
		var data = this.responseText;
		var jdata = JSON.parse(data);
		InventoryUpdateView.helpers.refreshInventoryViews(jdata,inventoryID,action);
	};
	ajax.open('POST', globals.SITE_URL+'/inventory/getInventoryItem/'+inventoryID+'/json/0');
	ajax.send({
		'authKey'  : globals.AUTH_KEY
	});
}	

Inventory.getInventoryAvatarImages = function(propertyID,pObj) {
	globals.ai._show({message: 'Getting inventory...'});
	var ajax = Titanium.Network.createHTTPClient();
	ajax.onerror = function(e) {
		alert('Error');
	};
	ajax.onload = function() {
		var data = this.responseText;
		var jdata = JSON.parse(data);
		pObj.InvAvatars = jdata;
		Property.renderRoomRows(propertyID,pObj);
	};
	ajax.open('POST', globals.SITE_URL+'/inventory/getInventoryAvatarImages/'+propertyID+'/json/0');
	ajax.send({
		'authKey'  : globals.AUTH_KEY
	});
}	

Inventory.getCategories = function() {
	var ajax = Titanium.Network.createHTTPClient();
	ajax.onerror = function(e) {
		alert('Error');
	};
	ajax.onload = function() {
		var data = this.responseText;
		var jdata = JSON.parse(data);
		globals.categories = jdata;
	};
	ajax.open('POST', globals.SITE_URL+'/inventory/getInventoryCategories');
	ajax.send({
		'authKey'  : globals.AUTH_KEY
	});
}

Inventory.getInventoryFile = function(fileID) {
	var ajax = Titanium.Network.createHTTPClient();
	ajax.onerror = function(e) {
		alert('Error');
	};
	ajax.onload = function() {
		var data = this.responseText;
		var jdata = JSON.parse(data);
		DocumentView.renderFile(jdata);
	};
	ajax.open('POST', globals.SITE_URL+'/filemanager/getFileItem/'+fileID+'/json/0');
	ajax.send({
		'authKey'  : globals.AUTH_KEY
	});
}

Inventory.deleteInventoryItem = function(inventoryID) {
	var ajax = Titanium.Network.createHTTPClient();
	ajax.onerror = function(e) {
		alert('Error');
	};
	ajax.onload = function() {
		var data = this.responseText;
		var jdata = JSON.parse(data);
		DocumentView.renderFile(jdata);
	};
	ajax.open('POST', globals.SITE_URL+'/inventory/deleteInventoryItem/'+inventoryID);
	ajax.send({
		'authKey'  : globals.AUTH_KEY
	});
}
