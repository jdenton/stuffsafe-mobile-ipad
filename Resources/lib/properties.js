Properties = {};

Properties.getProperties = function() {
	globals.ai._show({message: 'Getting properties...'});
	var ajax = Titanium.Network.createHTTPClient();
	ajax.onerror = function(e) {
		alert('Error');
	};
	ajax.onload = function() {
		var data = this.responseText;
		var jdata = JSON.parse(data);
		Properties.collection = jdata.Places;
		
		var propertyData = [], pObj, lPTitle, lAvatar, lDetails, lCount, row;
		var propertyDataDashboard = [], lPTitleDashboard, lAvatarDashboard, lTotalValueDashboard, lCreatedDashboard, lAddressDashboard, lPropertyDetails, lMap, rowDashboard;
		for (var pIndex = 0; pIndex < (jdata.Places.length); pIndex++ ) {
			pObj = jdata.Places[pIndex];
			pObj.Avatar = Properties.getPropertyAvatar(pObj.AvatarClass);
			row = Ti.UI.createTableViewRow({height: 60, hasChild: true, rooms: pObj.Rooms, propertyID: pObj.PlaceID, propertyName: pObj.PlaceName });
			
			lPTitle  = Ti.UI.createLabel({text: pObj.PlaceName, top: 8, left: 65, font: {fontSize: 18, fontWeight: 'bold'}});
			lAvatar = Ti.UI.createLabel({
				backgroundImage: pObj.Avatar,
				width: 48,
				height: 48,
				left: 8
			});
			lDetails = Ti.UI.createLabel({
				color: '#464c4d',
				left: 65,
				top: 32,
				font: {
					fontSize: 12
				},
				text: pObj.Address+' '+pObj.City+' '+pObj.State+' '+pObj.Zip
			});
			row.add(lPTitle,lDetails,lAvatar);
			propertyData.push(row);
			
			/*
			 * Create dashboard rows
			 */		
			propertyDataDashboard.push(Property.renderPropertyRow(pObj));
			
		}	
		setTimeout(function() {
			Dashboard.tblProperties.setData(propertyDataDashboard);
			Dashboard.tblProperties.setHeight(jdata.Places.length*100);
		}, 1000);	
		
		Dashboard.tblProperties.addEventListener('click', function(e) {
			var row = e.row;
			var rowdata = e.rowData;
			var rowIndex = e.index;
			if (e.source.id == 'map') {
				MapModalWindow.init(e.source.pObj);
				MapModalWindow.win.open({
					modal: true,
					modalStyle: Titanium.UI.iPhone.MODAL_PRESENTATION_FORMSHEET
				});
			} else {
				AppInit.showRoomView(e);
			}	
		});
		
		AppInit.mainMenuView.setData(propertyData);
	};
	ajax.open('POST', globals.SITE_URL+'places/getPlaces/0/json/0');
	ajax.send({
		'authKey'  : globals.AUTH_KEY
	});
}

Properties.getPropertyAvatar = function(avatarClass) {
	var avatar;
	switch (avatarClass) {
		case 'home':
			avatar = 'iconPropertyHome.png';
			break;
		case 'apartment':
			avatar = 'iconPropertyApt.png';
			break;	
		case 'multi':
			avatar = 'iconPropertyMultiFamily.png';
			break;	
		case 'townhouse':
			avatar = 'iconPropertyMultiFamily.png';
			break;	
		case 'condominium':
			avatar = 'iconPropertyCondo.png';
			break;	
		case 'business':
			avatar = 'iconPropertyBusiness.png';
			break;	
		case 'plane':
			avatar = 'iconPropertyPlane.png';
			break;
		case 'boat':
			avatar = 'iconPropertyBoat.png';
			break;
		default:
			avatar = 'iconPropertyHome.png';		
	}
	return 'images/'+avatar;
}
