Reports = {};

Reports.getRptInventoryByCategory = function() {
	globals.ai._show({message: 'Getting data...'});
	var ajax = Titanium.Network.createHTTPClient();
	ajax.onerror = function(e) {
		alert('Error');
	};
	ajax.onload = function() {
		var data = this.responseText;
		var jdata = JSON.parse(data);
		var chartTitle = jdata.ChartData.title.text;
		var chartData  = jdata.ChartData.elements[0].values;
				
		var data = [], labels = [], complete = [];
		for(var dp in chartData) {
			var el = [];
			data.push(chartData[dp].value);
			labels.push(chartData[dp].label);
			el.push(0);
			el.push(chartData[dp].value);
			complete.push({ 'data': [el], 'label': chartData[dp].label })
		}
		var chartDataRender = {
			data:   data,
			labels: labels,
			complete: complete,
			title:  chartTitle,
			grandTotal: jdata.GrandTotal
		}	
		globals.chart1Data = chartDataRender;
	};
	ajax.open('POST', globals.SITE_URL+'reports/getReportValueByCategory/json/0');
	ajax.send({
		'authKey'  : globals.AUTH_KEY
	});
}

Reports.getRptPurchaseReplacementByCategory = function() {
	globals.ai._show({message: 'Getting data...'});
	var ajax = Titanium.Network.createHTTPClient();
	ajax.onerror = function(e) {
		alert('Error');
	};
	ajax.onload = function() {
		var data = this.responseText;
		var jdata = JSON.parse(data);
		var chartTitle = jdata.ChartData.title.text;
		var chartSeries1 = jdata.ChartData.elements[0].values;
		var chartSeries2 = jdata.ChartData.elements[1].values;
		var labels       = jdata.ChartData.x_axis.labels.labels;
				
		var chartDataRender = {
			dataSeries1:  chartSeries1,
			dataSeries2:  chartSeries2,
			title:  chartTitle,
			labels: labels
		}	
		globals.chart2Data = chartDataRender;		
		
		setTimeout(function() {
		    Ti.App.fireEvent('renderChart', globals.chart1Data);
			Ti.App.fireEvent('renderChart2', globals.chart2Data);
			Ti.App.fireEvent('renderChart3', globals.chart2Data);	
		}, 1000);
	};
	ajax.open('POST', globals.SITE_URL+'reports/getReportPurchaseReplace/category/json/0');
	ajax.send({
		'authKey'  : globals.AUTH_KEY
	});
}
