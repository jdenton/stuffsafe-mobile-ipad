Styles = {}

Styles.gradient = function(color) {
	var colorStart, colorEnd;
	switch(color) {
		case 'green':
			colorStart = '#c6ec54';
			colorEnd   = '#9acc01';
			break;
		case 'red':
			colorStart = '#faa3a2';
			colorEnd   = '#f21413';
			break;
		case 'ltGray':
			colorStart = '#f5f5f5';
			colorEnd   = '#e2dede';
			break;
		case 'mdGray':
			colorStart = '#f2f2f2';
			colorEnd   = '#ccc9c9';
			break;
		case 'dkGray':
			colorStart = '#464c4d';
			colorEnd   = '#252627';
			break;
		case 'ltBlue':
			colorStart = '#ebf4fd';
			colorEnd   = '#7ebcf7';
			break;
		default:
			colorStart = '#ebf4fd';
			colorEnd   = '#7ebcf7';
			break;
	}
	
	var style = {
		type: 'linear',
		startPoint: { x: '0%', y: '0%' },
		endPoint: { x: '0%', y: '100%' },
		colors: [{ color: colorStart, offset: 0.0},{color: colorEnd, ofset: 0.5}]
	}
	
	return style;
}

Styles.navBarButton = function(title,color) {
	var borderColor, txtColor;
	switch(color) {
		case 'mdGray':
			borderColor = '#ccc9c9';
			txtColor = '#a3a1a1';
			break;
		case 'ltBlue':
			borderColor = '#3787d3';
			txtColor = '#2873ba';
			break;	
	}
	
	var style = {
		borderColor: borderColor,	    
	    color: txtColor,
	    backgroundImage: 'none',
	    borderRadius: 5,
	    borderWidth: 1,
	    height: 30,
	    font:{ fontSize: 13 },
	    width: 50,
	    title: title,
	    backgroundGradient: globals.styles.gradient(color)
	}
	return style;    
}

Styles.tableViewBubble = function(text) {
	var style = {
		backgroundColor: '#8fa2c1',
		borderColor: '#8fa2c1',
		borderRadius: 11,
		left: 240,
		height: 23,
		width: 30,
		color: '#fff',
		textAlign: 'center',
		text: text,
		font:{ fontSize: 18, fontWeight: 'bold' },
		shadowColor: '#758bad',
	    shadowOffset:{ x:1, y:1}
	}
	return style;
}