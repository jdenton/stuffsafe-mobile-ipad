Dashboard = {};
Dashboard.view = Ti.UI.createWindow();

Dashboard.init = function() {
	globals.roomID = null;
	
	var wViewDashboardChart1 = Ti.UI.createWebView({  
		touchEnabled: false,
	    width: '100%',  
	    height: 400,  
	    left: 0,  
	    top: 0,  
	    url: 'ui/dashboardChart1.html'  
	});  
	
	var wViewDashboardChart2 = Ti.UI.createWebView({ 
		touchEnabled: false,
	    width: '100%',  
	    height: 400,  
	    left: 0,  
	    top: 0,  
	    url: 'ui/dashboardChart2.html'  
	});  
	
	var wViewDashboardChart3 = Ti.UI.createWebView({  
		touchEnabled: false,
	    width: '100%',  
	    height: 400,  
	    left: 0,  
	    top: 0,  
	    url: 'ui/dashboardChart3.html',
	    background: 'transparent'
	}); 
		
	var scrollableCharts = Ti.UI.createScrollableView({
		backgroundColor: '#fff',
		views: [wViewDashboardChart1, wViewDashboardChart2, wViewDashboardChart3],
		showPagingControl: true,
		pagingControlColor: '#bddbf9',
		pagingControlHeight: 30,
		left: 0,
		top: 0,
		height: 400,
		width: '100%'
	});
	
	var scrollableProperties = Ti.UI.createScrollView({
		contentWidth: 'auto', 
		contentHeight: 'auto', 
		top: 400, 
		left: 0,
		showVerticalScrollIndicator: true, 
		showHorizontalScrollIndicator: false 
	});
	
	Dashboard.tblProperties = Ti.UI.createTableView({
		top: 0
	});	
	scrollableProperties.add(Dashboard.tblProperties);
	
	Dashboard.view.add(scrollableCharts,scrollableProperties);		
}