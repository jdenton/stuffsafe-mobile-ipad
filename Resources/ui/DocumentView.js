DocumentView = {};

DocumentView.init = function(fileObj) {
	InventoryModalWindow.toggleModalToolbar(false);
	
	DocumentView.view = Ti.UI.createScrollView({
		top: 0,
		left: 0,
		contentHeight: 'auto',
		contentWidth: 'auto',
		showHorizontalScrollIndicator: true,
		showVerticalScrollIndicator: true
	});
	DocumentView.docView = Ti.UI.createImageView({
		width: 540,
		height: 600,
		backgroundColor: '#eeefe1'
	});	
	DocumentView.view.add(DocumentView.docView);
	if (fileObj.fileLink) {
		DocumentView.renderFile(fileObj);
	} else {
		Inventory.getInventoryFile(fileObj.fileID);
	}	
}

DocumentView.renderFile = function(fileObj) {
	/*
	var filename = 'The_iPad_App_Wave.pdf';
    var f = Titanium.Filesystem.getFile(Titanium.Filesystem.applicationDataDirectory,filename);
	var docWindow = Ti.UI.iPad.createDocumentViewer({
		url: f.nativePath
	});
	docWindow.show();
	*/	
	DocumentView.docView.image = fileObj.fileLink;
}
