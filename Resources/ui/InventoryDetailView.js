InventoryDetailView = {};

InventoryDetailView.init = function(invObj) {
	InventoryDetailView.view = Ti.UI.createScrollView({
		contentWidth: 500,
		contentHeight: 'auto', 
		top: 0, 
		showVerticalScrollIndicator: false, 
		showHorizontalScrollIndicator: false 
	});
	
	var imgAvatar = Ti.UI.createImageView({
		image: invObj.AvatarURL,
		width: 120,
		height: 120,
		left: 12,
		top: 12,
		borderRadius: 8
	});
	
	var viewPricePurchase = Ti.UI.createView({
		backgroundImage: 'images/priceTagGreen.png',
		width: 125,
		height: 52,
		left: 144,
		top: 6
	});
	
	var viewPriceReplace = Ti.UI.createView({
		backgroundImage: 'images/priceTagOrange.png',
		width: 125,
		height: 52,
		left: 330,
		top: 6
	});
	
	var lblPricePurchase = Ti.UI.createLabel({
		text: '$'+invObj.PricePurchase,
		textAlign: 'right',
		font: { fontSize: 16, fontWeight: 'bold' },
		top: 9,
		left: 5,
		width: 88,
		height: 27
	});
	
	var lblPriceReplace = Ti.UI.createLabel({
		text: '$'+invObj.PriceReplace,
		textAlign: 'right',
		font: { fontSize: 16, fontWeight: 'bold' },
		top: 9,
		left: 5,
		width: 88,
		height: 27
	});
	
	viewPricePurchase.add(lblPricePurchase);
	viewPriceReplace.add(lblPriceReplace);
	
	var lblMake = Ti.UI.createLabel({
		text: 'Make: '+invObj.ItemMake,
		font: { fontSize: 14, fontWeight: 'bold' },
		width: 170,
		height: 20,
		left: 154,
		top: 69
	});
	
	var lblModel = Ti.UI.createLabel({
		text: 'Model: '+invObj.ItemModel,
		font: { fontSize: 14, fontWeight: 'bold' },
		width: 170,
		height: 20,
		left: 340,
		top: 69
	});
	
	var lblCategory = Ti.UI.createLabel({
		text: invObj.MainCat,
		font: { fontSize: 14, fontWeight: 'bold' },
		width: 170,
		height: 20,
		left: 154,
		top: 92
	});
	
	var lblRoom = Ti.UI.createLabel({
		text: invObj.RoomName,
		font: { fontSize: 14, fontWeight: 'bold' },
		width: 170,
		height: 20,
		left: 340,
		top: 92
	});
	
	var lblCondition = Ti.UI.createLabel({
		text: invObj.ItemCondition.toUpperCase()+' condition',
		font: { fontSize: 14, fontWeight: 'bold' },
		width: 170,
		height: 20,
		left: 154,
		top: 115
	});
	
	var lblSN = Ti.UI.createLabel({
		text: 'SN: '+invObj.ItemSerial,
		font: { fontSize: 14, fontWeight: 'bold' },
		width: 150,
		height: 20,
		left: 340,
		top: 115
	});
	
	var lblDateEntered = Ti.UI.createLabel({
		text: 'Entered on '+invObj.DateEnteredHuman+' by '+invObj.EnteredBy,
		font: { fontSize: 14, fontStyle: 'italics' },
		color: '#999',
		width: 400,
		height: 20,
		left: 12,
		top: 144
	});
	
	var lblDescHeight = Utils.setLabelHeight(invObj.ItemDesc,510,14);
	var lblDescription = Ti.UI.createLabel({
		text: Utils.txtToField(invObj.ItemDesc),
		font: { fontSize: 14 },
		width: 510,
		height: lblDescHeight,
		left: 12,
		top: 164
	});
	
	/*
	 * Display the item photos
	 */
	var viewFiles = Ti.UI.createView({
		width: 510,
		left: 0,
		top: parseInt(164+lblDescHeight+12)
	});
	if (invObj.Files.length > 0) {
		var jsonObj = invObj.Files;
	
		var fileData = [];
		var totalRows = Math.ceil(jsonObj.length/2);
	
		var row, cell, left = 12, top = 12, count = 0, cellWidth = 250, cellHeight = 230;
		var lblDesc, lblFile, scrollView;
		var padding = 12;
		
		var imageCenterX = Math.ceil(cellWidth/2);
		var imageCenterY = parseInt(Math.ceil((cellHeight-30)/2)+padding);
		
		for (var a=1; a<=totalRows; a++) {		
			row = Ti.UI.createView({
				top: top,
				height: cellHeight
			});
		
			left = padding;
			for (var b=0; b<=1; b++) {
				if (typeof jsonObj[count] != 'undefined') {
					var itemObj = jsonObj[count];
			
					cell = Ti.UI.createView({
						left: left,
						backgroundColor: '#eee',
						borderRadius: 8,
						height: cellHeight,
						width: cellWidth,
						id: itemObj.FileID
					});  
					
					if (itemObj.FileDesc == '0') {
						itemObj.FileDesc = '';
					}
					lblDesc = Ti.UI.createLabel({
						text: itemObj.FileDesc,
						width: cellWidth-padding,
						left: 12,
						width: 188,
						height: 20,
						top: 206,
						font: { fontSize: 14 },
						id: itemObj.FileID
					});
					
					lblFile = Ti.UI.createImageView({
						image: itemObj.fileLink,
						width: 200,
						height: 200,
						center: { x: imageCenterX, y: imageCenterY },
						borderRadius: 6,
						id: itemObj.FileID
					});
					
					cell.add(lblFile,lblDesc);
					row.add(cell);
					
					cell.addEventListener('click', function() {
						DocumentView.init(itemObj);
						InventoryModalWindow.win.animate({view: DocumentView.view, transition: Ti.UI.iOS.ANIMATION_CURVE_EASE_IN});
						InventoryModalWindow.win.setLeftNavButton();
						InventoryModalWindow.win.setRightNavButton(InventoryModalWindow.buttons.btnDetailImageWindowClose);
					});
					
					left = parseInt(left+cellWidth+padding);
					count++;			
				}	
			}
			top = parseInt(top+cellHeight+padding);
			viewFiles.add(row);
		}	
	}
		
	InventoryDetailView.view.add(
		imgAvatar,
		viewPricePurchase,
		viewPriceReplace,
		lblMake,
		lblModel,
		lblCategory,
		lblRoom,
		lblCondition,
		lblSN,
		lblDateEntered,
		lblDescription,
		viewFiles
	);	
}

InventoryDetailView.helpers = {
	switchFromImageToDetailView : function() {
		InventoryModalWindow.win.animate({view: InventoryDetailView.view, transition: Ti.UI.iOS.ANIMATION_CURVE_EASE_IN});
		InventoryModalWindow.win.setLeftNavButton(InventoryModalWindow.buttons.btnInvWindowEdit);
		InventoryModalWindow.win.setRightNavButton(InventoryModalWindow.buttons.btnInvWindowClose);
	}
}	
