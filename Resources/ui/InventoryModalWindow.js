/*
 * InventoryModalWindow.js
 * =========================================
 * This file provides the modal window for viewing 
 * an inventory item detail, or adding/editing an
 * inventory item.
 */

InventoryModalWindow = {};

/*
 * This will be a collection of buttons for the 
 * modal window and its various states of being.
 */
InventoryModalWindow.buttons = {};

var ui = {};
InventoryModalWindow.win = Ti.UI.createWindow({ 
	barColor: '#8cc5f2',
	backgroundColor: '#fff'
});

var flexSpace = Titanium.UI.createButton({
	systemButton:Titanium.UI.iPhone.SystemButton.FLEXIBLE_SPACE
});
	
var trash = Ti.UI.createButton({
	systemButton:Titanium.UI.iPhone.SystemButton.TRASH
});

var dlgTrash = Titanium.UI.createOptionDialog({
	options:['Delete','Cancel',''],
	destructive: 0,
	cancel: 1
});

trash.addEventListener('click', function() {
	dlgTrash.show({view: trash, animated: true});
});

dlgTrash.addEventListener('click', function(e) {
	if (e.index == 0) {
		var inventoryID = InventoryModalWindow.invObj.InventoryID;
		Inventory.deleteInventoryItem(inventoryID);
		InventoryModalWindow.win.close();
		
		for (var a=0;a<=Inventory.collection.length-1;a++) {
			if (Inventory.collection[a].InventoryID == inventoryID) {
				Inventory.collection.splice(a,1);
				InventoryUpdateView.helpers.updateRoomItemCount('decrement');
				InventoryView.renderItems();
			}
		}	
	}	
});

InventoryModalWindow.init = function(invObj,action) {
	createModalButtons();
	InventoryModalWindow.action = action;
	InventoryModalWindow.invObj = invObj;
	
	/*
	 * Action: view, edit, add
	 */
	if (action == 'view') {
		InventoryDetailView.init(invObj);
		InventoryModalWindow.win.animate({view: InventoryDetailView.view, transition: Ti.UI.iOS.ANIMATION_CURVE_EASE_IN});
		InventoryModalWindow.win.title = Utils.txtToField(invObj.ItemName);
		InventoryModalWindow.win.setRightNavButton(InventoryModalWindow.buttons.btnInvWindowClose);		
		InventoryModalWindow.win.setLeftNavButton(InventoryModalWindow.buttons.btnInvWindowEdit);
		InventoryModalWindow.toggleModalToolbar(true);
	} else if (action == 'add') {
		InventoryModalWindow.win.title = 'Add new item';
		openEditView(null,action);
	} else if (action == 'fileView') {
		DocumentView.init(invObj);
		InventoryModalWindow.win.animate({view: DocumentView.view, transition: Ti.UI.iOS.ANIMATION_CURVE_EASE_IN});
		InventoryModalWindow.win.setRightNavButton(InventoryModalWindow.buttons.btnInvWindowClose);
	}
}

function openEditView(action) {
	var invItemObj = InventoryModalWindow.invObj;
	InventoryModalWindow.action = action;
	InventoryModalWindow.win.setLeftNavButton(InventoryModalWindow.buttons.btnInvWindowSave);
	InventoryModalWindow.win.setRightNavButton(InventoryModalWindow.buttons.btnCancelEdit);
	
	if (globals.UPDATE_FORM_INIT === false) {
		InventoryUpdateView.buildUI.init();
	}	
	InventoryModalWindow.win.animate({view: InventoryUpdateView.view, transition: Ti.UI.iOS.ANIMATION_CURVE_EASE_IN});
	
	InventoryUpdateView.helpers.initForm(invItemObj,action);
}

function createModalButtons() {
	/*
	 * (btnInvWindowClose) Item Detail Close
	 */
	InventoryModalWindow.buttons.btnInvWindowClose = Ti.UI.createButton({
		title: 'Close',
		style: Titanium.UI.iPhone.SystemButtonStyle.BORDERED
	});
	InventoryModalWindow.buttons.btnInvWindowClose.addEventListener('click', function() {
		InventoryModalWindow.win.close();
	});
	
	/*
	 * (btnInvWindowEdit) Item Edit
	 */
	InventoryModalWindow.buttons.btnInvWindowEdit = Ti.UI.createButton({
		title: 'Edit',
		image: 'images/iconEdit_16.png',
		style: Titanium.UI.iPhone.SystemButtonStyle.BORDERED
	});
	InventoryModalWindow.buttons.btnInvWindowEdit.addEventListener('click', function() {
		openEditView('edit');
	});
	
	/*
	 * (btnCancelEdit) Item Cancel Edit
	 */
	InventoryModalWindow.buttons.btnCancelEdit = Ti.UI.createButton({
		title: 'Cancel'
	});
	InventoryModalWindow.buttons.btnCancelEdit.addEventListener('click', function() {
		if (InventoryModalWindow.action == 'view' || InventoryModalWindow.action == 'edit') {
			InventoryModalWindow.win.animate({view: InventoryDetailView.view, transition: Ti.UI.iOS.ANIMATION_CURVE_EASE_IN});
		} else {
			InventoryModalWindow.win.close();
		}
		InventoryModalWindow.win.setLeftNavButton(InventoryModalWindow.buttons.btnInvWindowEdit);
		InventoryModalWindow.win.setRightNavButton(InventoryModalWindow.buttons.btnInvWindowClose);
		
		InventoryUpdateView.helpers.clearForm();
	});
	
	/*
	 * (btnInvWindowSave) Item Save
	 */
	InventoryModalWindow.buttons.btnInvWindowSave = Ti.UI.createButton({
		title: 'Save',
		style:Titanium.UI.iPhone.SystemButtonStyle.DONE
	});	
	InventoryModalWindow.buttons.btnInvWindowSave.addEventListener('click', function() {
		InventoryUpdateView.dataObj.saveInventoryItem();
	});
	
	/*
	 * (btnImageWindowClose) Close Image View Window
	 */
	InventoryModalWindow.buttons.btnImageWindowClose = Ti.UI.createButton({
		title: 'Close'
	});
				
	InventoryModalWindow.buttons.btnImageWindowClose.addEventListener('click', function() {
		InventoryUpdateView.helpers.switchFromImageToEditView();
		InventoryModalWindow.toggleModalToolbar(true);
	});
	
	/*
	 * (btnDetailImageWindowClose) Close Image View Window
	 */
	InventoryModalWindow.buttons.btnDetailImageWindowClose = Ti.UI.createButton({
		title: 'Close'
	});
				
	InventoryModalWindow.buttons.btnDetailImageWindowClose.addEventListener('click', function() {
		InventoryDetailView.helpers.switchFromImageToDetailView();
		InventoryModalWindow.toggleModalToolbar(true);
	});
	
	/*
	 * (btnImageRemove) Remove image from item
	 */
	InventoryModalWindow.buttons.btnImageRemove = Titanium.UI.createButtonBar({
		labels: ['Remove'], 
		backgroundColor: 'red',
		style: Titanium.UI.iPhone.SystemButtonStyle.BAR
	});
	/*
	 * Event handler located in InventoryUpdateView.js
	 */
}

InventoryModalWindow.toggleModalToolbar = function(show) {
	if (show == true) {
		InventoryModalWindow.win.setToolbar([flexSpace,trash,flexSpace]);
	} else {
		InventoryModalWindow.win.setToolbar(null,{animated: true});
	}
}
