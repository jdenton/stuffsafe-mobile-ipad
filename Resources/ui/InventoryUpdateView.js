InventoryUpdateView = {};
var ui = {};
var datePickerValue = '';
var mediaCollection = [];
var uploadCollection = [];
var removeImageIndex;
InventoryUpdateView.inventoryID = 0;
InventoryUpdateView.action = 'add';

InventoryUpdateView.view = Ti.UI.createView();
InventoryUpdateView.buildUI = {
	init : function() {
		globals.UPDATE_FORM_INIT = true;
		InventoryUpdateView.buildUI.buildPopovers();
		InventoryUpdateView.buildUI.buildFormTable();
		InventoryUpdateView.attachEventHandlers.formFieldHandlers();
		InventoryUpdateView.attachEventHandlers.buttonHandlers();
	}, 
	buildUIControls : function() {
	},
	buildPopovers : function() {
		/*
		 * Category select popover
		 */
		ui.poCat = Ti.UI.iPad.createPopover({ 
			height: 300, 
			width: 250,
			title: 'Select Category',
			arrowDirection: Ti.UI.iPad.POPOVER_ARROW_DIRECTION_LEFT
		});
		
		/*
		 * Room select popover
		 */
		ui.poRoom = Ti.UI.iPad.createPopover({ 
			height: 300, 
			width: 250,
			title: 'Select Room',
			arrowDirection: Ti.UI.iPad.POPOVER_ARROW_DIRECTION_LEFT
		});
		
		ui.poCondition = Ti.UI.iPad.createPopover({ 
			height: 175, 
			width: 200,
			title: 'Condition',
			arrowDirection: Ti.UI.iPad.POPOVER_ARROW_DIRECTION_LEFT
		});
		
		var data = [
			{title: 'Excellent', value: 'excellent'},
			{title: 'Good', value: 'good'},
			{title: 'Fair', value: 'fair'},
			{title: 'Poor', value: 'poor'}
		];
		ui.conditionTable = Ti.UI.createTableView({data:data});
		ui.poCondition.add(ui.conditionTable);
	
		ui.btnCancelCal = Ti.UI.createButton({
			title: 'Cancel',
			style: Ti.UI.iPhone.SystemButtonStyle.BORDERED
		});	
	
		ui.btnDoneCal = Ti.UI.createButton({
			title: 'Done',
			style: Ti.UI.iPhone.SystemButtonStyle.DONE
		});
		ui.calPicker = Ti.UI.createPicker({
			type:Ti.UI.PICKER_TYPE_DATE
		});
		ui.poDatePurchase = Ti.UI.iPad.createPopover({ 
			height: 175, 
			width: 290,
			title: 'Purchase Date',
			arrowDirection: Ti.UI.iPad.POPOVER_ARROW_DIRECTION_LEFT,
			rightNavButton: ui.btnDoneCal,
			leftNavButton: ui.btnCancelCal
		});
		
		ui.poDatePurchase.add(ui.calPicker);
		
	},
	buildFormTable : function() {
		data = [];
		
		var tableView = Ti.UI.createTableView({
			style: Titanium.UI.iPhone.TableViewStyle.GROUPED,
			backgroundColor:'transparent',
			rowBackgroundColor:'white'
		});
		
		ui.fieldTitle = Ti.UI.createTextField({
			hintText: 'Title',
			left: 8,
			height: 40,
			width: 467,
			clearButtonMode: Ti.UI.INPUT_BUTTONMODE_ONFOCUS,
			keyboardType: Titanium.UI.KEYBOARD_DEFAULT,
			returnKeyType: Titanium.UI.RETURNKEY_DONE
		});
		
		ui.fieldRoom = Ti.UI.createTextField({
			hintText: 'Room',
			height: 40,
			width: 445,
			left: 30,
			clearButtonMode: Ti.UI.INPUT_BUTTONMODE_ONFOCUS
		});
		
		ui.fieldCategory = Ti.UI.createTextField({
			hintText: 'Category',
			height: 40,
			width: 445,
			left: 30,
			clearButtonMode: Ti.UI.INPUT_BUTTONMODE_ONFOCUS
		});
		
		ui.fieldDescription = Ti.UI.createTextArea({
			value: 'Description',
			color: '#bfbdbd',
			font: { fontSize: 17 },
			left: 25,
			height: 60,
			width: 450,
			keyboardType: Titanium.UI.KEYBOARD_DEFAULT
		});
		
		ui.fieldMake = Ti.UI.createTextField({
			hintText: 'Make',
			height: 40,
			width: 200,
			left: 30,
			clearButtonMode: Ti.UI.INPUT_BUTTONMODE_ONFOCUS,
			keyboardType: Titanium.UI.KEYBOARD_DEFAULT,
			returnKeyType: Titanium.UI.RETURNKEY_DONE
		});
		
		ui.fieldModel = Ti.UI.createTextField({
			hintText: 'Model',
			height: 40,
			width: 200,
			left: 260,
			clearButtonMode: Ti.UI.INPUT_BUTTONMODE_ONFOCUS,
			keyboardType: Titanium.UI.KEYBOARD_DEFAULT,
			returnKeyType: Titanium.UI.RETURNKEY_DONE
		});
		
		ui.fieldSN = Ti.UI.createTextField({
			hintText: 'Serial Number',
			height: 40,
			width: 445,
			left: 30,
			clearButtonMode: Ti.UI.INPUT_BUTTONMODE_ONFOCUS,
			keyboardType: Titanium.UI.KEYBOARD_DEFAULT,
			returnKeyType: Titanium.UI.RETURNKEY_DONE
		});
		
		ui.fieldDate = Ti.UI.createTextField({
			hintText: 'Purchase Date',
			editable: false,
			height: 40,
			width: 200,
			left: 30
		});
		
		ui.fieldCondition = Ti.UI.createTextField({
			hintText: 'Condition',
			editable: false,
			height: 40,
			width: 200,
			left: 260
		});
		
		ui.fieldPricePurchase = Ti.UI.createTextField({
			hintText: 'Purchase Price',
			editable: false,
			height: 40,
			width: 200,
			left: 30,
			clearButtonMode: Ti.UI.INPUT_BUTTONMODE_ONFOCUS,
			keyboardType: Titanium.UI.KEYBOARD_NUMBER_PAD
		});
		
		ui.fieldPriceReplace = Ti.UI.createTextField({
			hintText: 'Replacement Price',
			editable: false,
			height: 40,
			width: 200,
			left: 260,
			clearButtonMode: Ti.UI.INPUT_BUTTONMODE_ONFOCUS,
			keyboardType: Titanium.UI.KEYBOARD_NUMBER_PAD
		});
		
		ui.viewMediaControls = Ti.UI.createView({
			height: 44
		});
		
		ui.btnBarImageSelect = Ti.UI.createButtonBar({
		    labels: ['Camera', 'Gallery'], 
		    style: Titanium.UI.iPhone.SystemButtonStyle.BAR,
		    height: 32,
		    width: 150,
		    left: 6,
		    top: 6,
		    backgroundColor: '#8cc5f2',
		});
		
		ui.uploaderBarView = Ti.UI.createView({
			height: 20,
			top: 12,
			left: 163
		});
		
		ui.uploaderBar = Ti.UI.createLabel({
			height: 20,
			borderRadius: 3,
			left: 0,
			top: 0,
			backgroundGradient: Styles.gradient('ltBlue'),
			borderColor: '#62a5e5',
			borderWidth: 1
		});
		ui.uploaderBarView.add(ui.uploaderBar);
		ui.viewMediaControls.add(ui.btnBarImageSelect,ui.uploaderBarView);
		
		ui.imgViewRow = Ti.UI.createScrollView({
			contentWidth: 'auto',
			contentHeight: 'auto',
			top: 4,
			showVerticalScrollIndicator: true,
			left: 0
		});
		
		var row = Ti.UI.createTableViewRow({height: 40});
		row.add(ui.fieldTitle);
		data.push(row);
		
		row = Ti.UI.createTableViewRow({height: 40});
		lblIcon = Ti.UI.createLabel({
			backgroundImage: 'images/iconRoomLtGray.png',
			width: 16,
			height: 16,
			top: 12,
			left: 8
		});
		row.add(lblIcon,ui.fieldRoom);
		data.push(row);
		
		row = Ti.UI.createTableViewRow({height: 40});
		lblIcon = Ti.UI.createLabel({
			backgroundImage: 'images/iconCategoryLtGray.png',
			width: 16,
			height: 16,
			top: 12,
			left: 8
		});
		row.add(lblIcon,ui.fieldCategory);
		data.push(row);
		
		row = Ti.UI.createTableViewRow({height: 70});
		lblIcon = Ti.UI.createLabel({
			backgroundImage: 'images/iconNotesLtGray.png',
			width: 16,
			height: 16,
			top: 12,
			left: 8
		});
		row.add(lblIcon,ui.fieldDescription);
		data.push(row);
		
		row = Ti.UI.createTableViewRow({height: 40});
		lblIcon = Ti.UI.createLabel({
			backgroundImage: 'images/iconMakeLtGray.png',
			width: 16,
			height: 16,
			top: 12,
			left: 8
		});
		lblIcon2 = Ti.UI.createLabel({
			backgroundImage: 'images/iconModelLtGray.png',
			width: 16,
			height: 16,
			top: 12,
			left: 230
		});
		row.add(lblIcon,ui.fieldMake,lblIcon2,ui.fieldModel);
		data.push(row);
		
		row = Ti.UI.createTableViewRow({height: 40});
		lblIcon = Ti.UI.createLabel({
			backgroundImage: 'images/iconSNLtGray.png',
			width: 16,
			height: 16,
			top: 12,
			left: 8
		});
		row.add(lblIcon,ui.fieldSN);
		data.push(row);
		
		row = Ti.UI.createTableViewRow({height: 40});
		lblIcon = Ti.UI.createLabel({
			backgroundImage: 'images/iconDollarsLtGray.png',
			width: 16,
			height: 16,
			top: 12,
			left: 8
		});
		lblIcon2 = Ti.UI.createLabel({
			backgroundImage: 'images/iconDollarsLtGray.png',
			width: 16,
			height: 16,
			top: 12,
			left: 230
		});
		row.add(lblIcon,ui.fieldPricePurchase,lblIcon2,ui.fieldPriceReplace);
		data.push(row);
		
		row = Ti.UI.createTableViewRow({height: 40});
		lblIcon = Ti.UI.createLabel({
			backgroundImage: 'images/iconCalendarLtGray.png',
			width: 16,
			height: 16,
			top: 12,
			left: 8
		});
		lblIcon2 = Ti.UI.createLabel({
			backgroundImage: 'images/iconPriorityLtGray.png',
			width: 16,
			height: 16,
			top: 12,
			left: 230
		});
		row.add(lblIcon,ui.fieldDate,lblIcon2,ui.fieldCondition);
		data.push(row);
		
		row = Ti.UI.createTableViewRow({height: 44});
		row.add(ui.viewMediaControls);
		data.push(row);
		
		row = Ti.UI.createTableViewRow({height: 200});
		row.add(ui.imgViewRow);
		data.push(row);
		
		tableView.setData(data);	
		InventoryUpdateView.view.add(tableView);
		
		ui.catView = Ti.UI.createScrollView();
		ui.catTable = Ti.UI.createTableView();
		var row, dataCat = [];
		for (var cat in globals.categories) {
			row = Ti.UI.createTableViewRow({
				title: globals.categories[cat].ItemName,
				value: globals.categories[cat].ItemID,
				height: 40
			});
			dataCat.push(row);
		}	
		ui.catTable.setData(dataCat);
		ui.catView.add(ui.catTable);
		
		ui.roomView = Ti.UI.createScrollView();
		ui.roomTable = Ti.UI.createTableView();
		var row2, dataRoom = [];
		for (var room in globals.rooms) {
			row2 = Ti.UI.createTableViewRow({
				title: globals.rooms[room].RoomName,
				value: globals.rooms[room].RoomID,
				height: 40
			});
			dataRoom.push(row2);
		}	
		ui.roomTable.setData(dataRoom);
		ui.roomView.add(ui.roomTable);
	}
}

InventoryUpdateView.attachEventHandlers = {
	buttonHandlers : function() {	
		ui.btnCancelCal.addEventListener('click', function() {
			ui.poDatePurchase.hide();
		});
		
		ui.btnDoneCal.addEventListener('click', function() {
			ui.poDatePurchase.hide();
		});
	},
	formFieldHandlers : function() {
		ui.fieldRoom.addEventListener('focus', function() {
			ui.poRoom.add(ui.roomView);
			ui.poRoom.show({ view: ui.fieldRoom });
		});
		
		ui.fieldRoom.addEventListener('blur', function() {
			ui.poRoom.hide();
		});
		
		ui.fieldCategory.addEventListener('focus', function() {
			ui.poCat.add(ui.catView);
			ui.poCat.show({ view: ui.fieldCategory });
		});
		
		ui.fieldCategory.addEventListener('blur', function() {
			ui.poCat.hide();
		});
		
		ui.fieldDescription.addEventListener('focus', function(e) {
			this.color = '#000';
			if (this.value == 'Description') {
				this.value = '';	
			}
		});
	
		ui.fieldDate.addEventListener('focus', function() {
			ui.fieldDate.blur();
			ui.poDatePurchase.show({ view: ui.fieldDate });
		});
	
		ui.fieldCondition.addEventListener('focus', function() {
			ui.fieldCondition.blur();
			ui.poCondition.show({ view: ui.fieldCondition });
		});
		ui.conditionTable.addEventListener('click', function(e) {
			var row = e.row;
			var rowdata = e.rowData;
			var rowIndex = e.index;
			var condition = rowdata.title;
			var conditionValue = rowdata.value;
			
			ui.fieldCondition.value = condition;
			InventoryUpdateView.condition = conditionValue;
			ui.poCondition.hide();
		});
	
		ui.btnBarImageSelect.addEventListener('click', function(e) {
			var btnIndex = e.index;
			if (btnIndex == 0) {
				InventoryUpdateView.imageObj.camera();
			} else if (btnIndex == 1) {
				InventoryUpdateView.imageObj.gallery();
			}
		});
		
		ui.catTable.addEventListener('click', function(e) {
			var row = e.row;
			var rowdata = e.rowData;
			var rowIndex = e.index;
			var catName = rowdata.title;
			var catID   = rowdata.value;
			
			ui.fieldCategory.value = catName;
			InventoryUpdateView.catID = catID;
			ui.poCat.hide();
		});	
		
		ui.roomTable.addEventListener('click', function(e) {
			var row = e.row;
			var rowdata = e.rowData;
			var rowIndex = e.index;
			var roomName = rowdata.title;
			var roomID   = rowdata.value;
			
			ui.fieldRoom.value = roomName;
			InventoryUpdateView.roomID = roomID;
			ui.poRoom.hide();
		});	
		
		ui.calPicker.addEventListener('change', function(e) {
			var raw = e.value.toLocaleString();
			var date = globals.moment(raw);
			datePickerValue = date.format('YYYY-MM-DD');
			ui.fieldDate.value = date.format('MMMM Do YYYY');
		});	
		
		ui.imgViewRow.addEventListener('click', function(e) {
			if ( !isNaN(parseInt(e.source.id)) ) {
				removeImageIndex = e.source.id;
				var fileObj = {};
				fileObj.fileLink = e.source.image;
				DocumentView.init(fileObj);
				InventoryModalWindow.win.animate({view: DocumentView.view, transition: Ti.UI.iOS.ANIMATION_CURVE_EASE_IN});
				InventoryModalWindow.win.setRightNavButton(InventoryModalWindow.buttons.btnImageWindowClose);
				InventoryModalWindow.win.leftNavButton = InventoryModalWindow.buttons.btnImageRemove;
			}	
		});
		
		InventoryModalWindow.buttons.btnImageRemove.addEventListener('click', function(e) {
			var btnIndex = e.index;
			if (btnIndex == 0) {
				mediaCollection.splice(removeImageIndex,1);
				uploadCollection.splice(removeImageIndex,1);
				InventoryUpdateView.helpers.switchFromImageToEditView();
				InventoryUpdateView.imageObj.addMediaToView();
			}
		});	
	}		
}			

InventoryUpdateView.dataObj = {
	saveInventoryItem : function() {
		if (ui.fieldTitle.value == '') {
			var alert = Ti.UI.createAlertDialog({
				message: 'Please enter a title first.',
				ok: 'Okay',
				title: 'Oops!'
			}).show();
		} else if (ui.fieldRoom.value == '') {
			var alert = Ti.UI.createAlertDialog({
				message: 'Please select a room first.',
				ok: 'Okay',
				title: 'Oops!'
			}).show();
		} else {
			InventoryUpdateView.dataObj.saveItemToServer(data);
		}	
	},
	saveItemToServer : function() {
		var action = InventoryUpdateView.action;
		var description = ''
		if (ui.fieldDescription.value != 'Description') {
			description = Utils.escapeString(ui.fieldDescription.value);
		}
		var title = Utils.escapeString(ui.fieldTitle.value);
		var room  = Utils.escapeString(ui.fieldRoom.value);
		var category = Utils.escapeString(ui.fieldCategory.value);
		
		if (InventoryUpdateView.roomID == null) {
			InventoryUpdateView.roomID = '';
		}
		if (InventoryUpdateView.catID == null) {
			InventoryUpdateView.catID = '';
		}
		if (InventoryUpdateView.condition == null) {
			InventoryUpdateView.condition = '';
		}
						
		var ajax = Titanium.Network.createHTTPClient();
		ajax.uploadCollection = uploadCollection;
		ajax.onerror = function(e) {
			alert('Error');
		};
		
		ajax.onload = function() {
			var data = this.responseText;
			var jdata = JSON.parse(data);
			var inventoryID = jdata.InventoryID;
			
			if (this.uploadCollection.length === 0) {
				InventoryUpdateView.helpers.clearForm();
				InventoryUpdateView.helpers.refreshInventoryViews(jdata,inventoryID,action);
			} else {
				InventoryUpdateView.dataObj.uploadMedia(inventoryID,this.uploadCollection,jdata,action);
			}	
		};
		ajax.open('POST', globals.SITE_URL+'inventory/saveInventoryItem');
		ajax.send({
			action          : InventoryUpdateView.action,
			invID           : InventoryUpdateView.inventoryID,
			save            : 1,
			placeID         : globals.propertyID,
			invRoomID       : InventoryUpdateView.roomID,
			invRoomNew      : room,
			invName         : title,
			invDescription  : description,
			invPricePurchase: ui.fieldPricePurchase.value,
			invPriceReplace : ui.fieldPriceReplace.value,
            invCategory     : InventoryUpdateView.catID,
            invMake         : ui.fieldMake.value,
            invModel        : ui.fieldModel.value,
            invSN           : ui.fieldSN.value,
            invDate         : datePickerValue,
            invCondition    : InventoryUpdateView.condition,
            invTags         : '',        
            authKey         : globals.AUTH_KEY
		});	
	},
	uploadMedia : function(inventoryID,uploadCollection,invObj,action) {
		if (inventoryID == null) {
			inventoryID = 0;
		}
		ui.uploaderBar.width = 0;
		/*
		 * Upload media files
		 */
		var a = 1;
		for(var media in uploadCollection) {
			var xhr = Ti.Network.createHTTPClient();
			
			xhr.onerror = function(e){
			    Ti.API.info('IN ERROR ' + e.error);
			    alert('Sorry, we could not upload your photo! Please try again.');
			};
 
			xhr.onload = function() {
			    var data = this.responseText;
			    if (a == uploadCollection.length) {
			    	Inventory.getInventoryItem(inventoryID,action);
			    	InventoryUpdateView.helpers.clearForm();
			    }	
			    a++;
			};
 
			xhr.onsendstream = function(e) {
				Ti.API.info('ONSENDSTREAM - PROGRESS: '+e.progress);
				var barWidth = Math.round(308 * e.progress);
				ui.uploaderBar.width = barWidth;
			};
			xhr.open('POST', globals.SITE_URL+'filemanager/uploadFileHandler/'+inventoryID+'/inventory');
			xhr.send({
				'authKey'    : globals.AUTH_KEY,
				'fromMobile' : 1,
			    'file'       : uploadCollection[media]
			});
		}
	}
}

InventoryUpdateView.imageObj = {
	camera : function() {
		Ti.Media.showCamera({
		    success:function(e) {
		    	mediaCollection.push(e);
		    	uploadCollection.push(e.media);
		        InventoryUpdateView.imageObj.addMediaToView();
		    },
		    cancel:function() {
		        // called when user cancels taking a picture
		    },
		    error:function(error) {
		        // called when there's an error
		        var a = Titanium.UI.createAlertDialog({title:'Camera'});
		        if (error.code == Titanium.Media.NO_CAMERA) {
		            a.setMessage('Please run this test on device');
		        } else {
		            a.setMessage('Unexpected error: ' + error.code);
		        }
		        a.show();
		    },
		    saveToPhotoGallery:true,
		    allowEditing: false,
		    animated: true,
		    autohide: true,
		    showControls: true,
		    mediaTypes: [Ti.Media.MEDIA_TYPE_VIDEO,Ti.Media.MEDIA_TYPE_PHOTO]
		});
	},
	gallery : function() {
		Ti.Media.openPhotoGallery({
        	success:function(e) {
        		mediaCollection.push(e);
        		uploadCollection.push(e.media);
        		InventoryUpdateView.imageObj.addMediaToView();
		    },
            cancel:function() {
            },
            error:function(err) {
				Ti.API.error(err);
            },
            saveToPhotoGallery: false,
		    allowEditing: false,
		    animated: true,
		    autohide: true,
		    showControls: true,
		    arrowDirection: Ti.UI.iPad.POPOVER_ARROW_DIRECTION_UP,
		    mediaTypes: [Ti.Media.MEDIA_TYPE_VIDEO,Ti.Media.MEDIA_TYPE_PHOTO],
		    popoverView: ui.btnBarImageSelect
       });
	},
	addMediaToView : function() {
		InventoryUpdateView.helpers.clearMediaPanel();
		var imagePadding = 12, imageLeft = 15, imageTop = 12, mediaCount = 0;
		
		for(var media in mediaCollection) {
			if (mediaCollection[media].mediaType == Ti.Media.MEDIA_TYPE_PHOTO || mediaCollection[media].mediaType == Ti.Media.MEDIA_TYPE_VIDEO) {
	        	if (mediaCount % 4 == 0 && mediaCount > 0) {
	        		/*
	        		 * Then we need a new row of pics
	        		 */
	        		imageTop = parseInt(imageTop+112);
	        		imageLeft = 15;
	        	} else if (mediaCount>0) {
	        		imageLeft = parseInt(imageLeft+115)
	        	}
	        	var mediaView;
	        	if (mediaCollection[media].mediaType == Ti.Media.MEDIA_TYPE_VIDEO) {
	        		mediaView = Ti.Media.createVideoPlayer({
					    top : imageTop,
					    left: imageLeft,
					    autoplay : false,
					    media: mediaCollection[media].media,
					    backgroundColor : 'blue',
					    borderRadius: 8,
					    height : 100,
					    width : 100,
					    mediaControlStyle : Titanium.Media.VIDEO_CONTROL_DEFAULT,
					    scalingMode : Titanium.Media.VIDEO_SCALING_ASPECT_FIT,
					    id: mediaCount
					});
	        	} else {
		           	mediaView = Ti.UI.createImageView({
		                width: 100,
		                height: 100,
		                left: imageLeft,
		                top: imageTop,
		                borderRadius: 8,
		                image: mediaCollection[media].media,
		                id: mediaCount
		            });
		        }    
	            mediaCount++;
	            ui.imgViewRow.add(mediaView);
	        }
	     }   
	}
}

InventoryUpdateView.helpers = {
	initForm : function(invItemObj,action) {
		if (action == 'edit') {
			InventoryUpdateView.action = 'edit';
			InventoryUpdateView.inventoryID = invItemObj.InventoryID;
			ui.fieldTitle.value = Utils.txtToField(invItemObj.ItemName);
			ui.fieldCategory.value = Utils.txtToField(invItemObj.MainCat);
			InventoryUpdateView.catID = invItemObj.CatID;
			ui.fieldDescription.value = Utils.txtToField(invItemObj.ItemDesc);
			
			ui.fieldMake.value = Utils.txtToField(invItemObj.ItemMake);
			ui.fieldModel.value = Utils.txtToField(invItemObj.ItemModel);
			ui.fieldSN.value = Utils.txtToField(invItemObj.ItemSerial);
			ui.fieldDate.value = Utils.txtToField(invItemObj.DatePurchaseHuman);
			ui.fieldCondition.value = Utils.txtToField(invItemObj.ItemCondition);
			InventoryUpdateView.condition = invItemObj.ItemCondition;
			ui.fieldPricePurchase.value = Utils.txtToField(invItemObj.PricePurchase);
			ui.fieldPriceReplace.value = Utils.txtToField(invItemObj.PriceReplace);
			
			/*
			 * Add any files to media collection
			 */
			var invMedia = invItemObj.Files;
			var mediaItem = {};
			for(var a=0; a<=invMedia.length-1;a++) {
				mediaItem = {
					fromServer: true, 
					mediaType : Ti.Media.MEDIA_TYPE_PHOTO,
					media     : invMedia[a].fileLink
				}
				mediaCollection.push(mediaItem);
			}
			InventoryUpdateView.imageObj.addMediaToView();
		}
		if (globals.roomID > 0) {
			/*
			 * Prepopulate the room because we're already in a room.
			 */
			for (var room in globals.rooms) {
				if (globals.roomID === globals.rooms[room].RoomID) {
					ui.fieldRoom.value = globals.rooms[room].RoomName;
					InventoryUpdateView.roomID = globals.roomID;
				}
			}	
		}
	},
	closePickers : function() {
		
	},
	updateRoomItemCount : function(upDown) {
		var propertyID = globals.propertyID;
		var roomID     = InventoryUpdateView.roomID;
		
		var pObj, rObj;
		for(var property in Properties.collection) {
			if (Properties.collection[property].PlaceID === propertyID) {
				pObj = Properties.collection[property];	
			}
		}	
		for(var room in pObj.Rooms) {
			if (pObj.Rooms[room].RoomID === roomID) {
				rObj = pObj.Rooms[room];				
			}
		}	
		var roomItemCount = parseInt(rObj.RoomItemCount);
		if (upDown == 'increment') {
			rObj.RoomItemCount = parseInt(roomItemCount+1);
		} else {
			rObj.RoomItemCount = parseInt(roomItemCount-1);
		}
	},
	clearForm : function() {
		if (InventoryModalWindow.action === 'add') {
			InventoryModalWindow.win.close();
		}	
		InventoryUpdateView.action = 'add';
		ui.fieldTitle.value = '';
		ui.fieldRoom.value = '';
		ui.fieldCategory.value = '';
		ui.fieldDescription.value = '';
		ui.fieldMake.value = '';
		ui.fieldModel.value = '';
		ui.fieldSN.value = '';
		ui.fieldDate.value = '';
		ui.fieldCondition.value = '';
		ui.fieldPricePurchase.value = '';
		ui.fieldPriceReplace.value = '';
		ui.uploaderBar.width = 0;
		InventoryUpdateView.helpers.clearMediaPanel();
		
		datePickerValue = '';
		mediaCollection.length = 0;
		uploadCollection.length = 0;
		mediaCount = 0;
	},
	clearMediaPanel : function() {
		/*
		 * Remove media from ui.imgViewRow
		 */
		if (ui.imgViewRow.children) {
	        for (var c = ui.imgViewRow.children.length - 1; c >= 0; c--) {
	            ui.imgViewRow.remove(ui.imgViewRow.children[c]);
	        }
	    }
	},
	switchFromImageToEditView : function() {
		InventoryModalWindow.win.animate({view: InventoryUpdateView.view, transition: Ti.UI.iOS.ANIMATION_CURVE_EASE_IN});
		InventoryModalWindow.win.setLeftNavButton(InventoryModalWindow.buttons.btnInvWindowSave);
		InventoryModalWindow.win.setRightNavButton(InventoryModalWindow.buttons.btnCancelEdit);
	},
	refreshInventoryViews : function(invObj,inventoryID,action) {
		InventoryModalWindow.init(invObj,'view');
		if (action == 'add') {
			Inventory.collection.push(invObj);
			InventoryView.renderItems();
			InventoryUpdateView.helpers.updateRoomItemCount('increment');
		} else if (action == 'edit') {
			/*
			 * Replace inventory item in Inventory.collection
			 */
			for (var a=0;a<=Inventory.collection.length;a++) {
				if (Inventory.collection[a].InventoryID == inventoryID) {
					Inventory.collection[a] = invObj;
					InventoryView.renderItems();
				}
			}
		}
	}
}
