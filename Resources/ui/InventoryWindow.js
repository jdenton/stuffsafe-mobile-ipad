InventoryView = {};

InventoryView.init = function(propertyID,roomID) {
	globals.roomID = roomID;
	Inventory.getInventory(propertyID,roomID);
}

InventoryView.renderItems = function() {
	InventoryView.view = null;
	InventoryView.view = Ti.UI.createView();
	var jsonObj = Inventory.collection;
	
	var inventoryData = [];
	var totalRows = Math.ceil(jsonObj.length/3);
	
	var row, cell, left = 12, top = 12, count = 0, cellWidth = 230, cellHeight = 87;
	var lblName, imgAvatar, scrollView;
	var padding = 12;
	for (var a=1; a<=totalRows; a++) {		
		row = Ti.UI.createView({
			top: top,
			height: cellHeight
		});
		
		left = padding;
		for (var b=0; b<=2; b++) {
			if (typeof jsonObj[count] != 'undefined') {
				var itemObj = jsonObj[count];
		
				cell = Ti.UI.createView({
					left: left,
					backgroundColor: '#fff',
					borderRadius: 8,
					height: cellHeight,
					width: cellWidth,
					id: itemObj.InventoryID
				});  
				
				lblName = Ti.UI.createLabel({
					text: Utils.txtToField(itemObj.ItemName),
					width: cellWidth-padding,
					left: 90,
					width: 130,
					height: 40,
					top: 6,
					font: { fontSize: 12, fontWeight: 'bold' },
					id: itemObj.InventoryID
				});
				
				imgAvatar = Ti.UI.createImageView({
					image: itemObj.AvatarURL,
					width: 75,
					height: 75,
					left: 6,
					top: 6,
					borderRadius: 6,
					id: itemObj.InventoryID
				});
				
				cell.addEventListener('click', function(e) {
					for (var inv in Inventory.collection) {
						if (Inventory.collection[inv].InventoryID == e.source.id) {
							var thisObj = Inventory.collection[inv];
						}
					}	
					
					InventoryModalWindow.init(thisObj,'view');
					InventoryModalWindow.win.open({
						modal: true,
						modalStyle: Titanium.UI.iPhone.MODAL_PRESENTATION_FORMSHEET
					});
				});
				
				cell.add(imgAvatar,lblName);
				row.add(cell);
				
				left = parseInt(left+cellWidth+padding);
				count++;			
			}	
		}
		top = parseInt(top+cellHeight+padding);
		InventoryView.view.add(row);
	}
	AppInit.detailWindow.animate({view: InventoryView.view, transition: Ti.UI.iOS.ANIMATION_CURVE_EASE_IN});
}
