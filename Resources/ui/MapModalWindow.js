/*
 * MapModalWindow.js
 * =========================================
 * This file provides the modal window for viewing 
 * a property map.
 */

MapModalWindow = {};
MapModalWindow.win = Ti.UI.createWindow({ 
	barColor: '#8cc5f2',
	backgroundColor: '#fff'
});

MapModalWindow.init = function(pObj) {
	var address = pObj.Address+' '+pObj.City+' '+pObj.State+' '+pObj.Zip;
	var propertyName = pObj.PlaceName;
	var avatar = pObj.Avatar;
	
	MapModalWindow.win.title = address;
		
	var btnMapWindowClose = Ti.UI.createButton({
		title: 'Close',
		style: Titanium.UI.iPhone.SystemButtonStyle.BORDERED
	});
	btnMapWindowClose.addEventListener('click', function() {
		MapModalWindow.win.close();
	});
	MapModalWindow.win.setRightNavButton(btnMapWindowClose);
	
	var mapView = Ti.Map.createView({
	    mapType: Ti.Map.STANDARD_TYPE,
	    animate: true,
	    regionFit: true,
	    userLocation: true
	});
	MapModalWindow.win.add(mapView);
	
	/*
	 * Ti.Geolocation.forwardGeocoder doesn't work anymore 
	 * so do the following...
	 */
	var xhrCoords = Ti.Network.createHTTPClient();
	xhrCoords.open('GET', 'http://maps.googleapis.com/maps/geo?output=json&q=' + address);
	xhrCoords.onload = function() {
	    var json = JSON.parse(this.responseText);
	    var lat = json.Placemark[0].Point.coordinates[1];
	    var long = json.Placemark[0].Point.coordinates[0];
	    Ti.API.info(lat+' '+long);
	    mapView.region = {latitude: lat, longitude: long, latitudeDelta:0.01, longitudeDelta:0.01}
	    
	    var mapAnnotation = Titanium.Map.createAnnotation({
		    latitude: lat,
		    longitude: long,
		    title: propertyName,
		    subtitle: address,
		    pincolor: Titanium.Map.ANNOTATION_RED,
		    animate: true,
		    leftButton: avatar,
		    myid: 1
		});
		mapView.annotations = [mapAnnotation];
	};
	xhrCoords.send();
}	