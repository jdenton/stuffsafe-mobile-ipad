exports.MenuWindow = function(args) {
	var instance = Ti.UI.createWindow(args);
	
	args.masterWindow.title = 'Menu';
	
	var label1 = Ti.UI.createLabel({
		top: 10,
		text: 'Timer'
	});
	
	var label2 = Ti.UI.createLabel({
		top: 40,
		text: 'Tasks'
	});
	
	label1.addEventListener('click', function() {
		args.detailNavGroup.window = globals.timerWindow;
	});
	
	label2.addEventListener('click', function() {
		args.detailNavGroup.window = globals.tasksWindow;
	});
	
	instance.add(label1,label2);
	
	
	
	return instance;
}