/*
 * PropertyWindow.js
 * =========================================
 * This file renders the property detail window
 * after clicking on a property from the main page.
 */

Property = {};
Property.init = function(propertyID) {
	Property.view = Ti.UI.createView();
	/*
	 * Get our property data collection
	 */
	var pObj;
	for(var property in Properties.collection) {
		if (Properties.collection[property].PlaceID === propertyID) {
			pObj = Properties.collection[property];	
		}
	}	
	
	Inventory.getInventoryAvatarImages(propertyID,pObj);
}

Property.renderRoomRows = function(propertyID,pObj) {
	var tableRoomsView = Ti.UI.createTableView({
		top: 120,
		left: 0
	});
	
	var avatarArray = pObj.InvAvatars;
	var rObj, row, rData = [], lTitle, lCount, sPhotos, url, lRoomStats;
	for (var rIndex = 0; rIndex < (pObj.Rooms.length); rIndex++ ) {
		rObj = pObj.Rooms[rIndex];
		
		row = Ti.UI.createTableViewRow({
			height: 75, 
			hasChild: false, 
			propertyID: propertyID, 
			propertyName: pObj.PlaceName, 
			roomID: rObj.RoomID, 
			roomName: rObj.RoomName 
		});
		lTitle = Ti.UI.createLabel({
			text: rObj.RoomName,
			top: 14, 
			left: 12, 
			font: {fontSize: 20, fontWeight: 'bold'}
		});
		//lCount = Ti.UI.createLabel(Styles.tableViewBubble(rObj.RoomItemCount));
		var itemText = 'items';
		if (rObj.RoomItemCount == 1) {
			itemText = 'item';
		}
		lRoomStats = Ti.UI.createLabel({
			text: rObj.RoomItemCount+' '+itemText+', $'+rObj.RoomTotal,
			font: { fontSize: 14 },
			color: '#464c4d',
			left: 12,
			top: 40	
		});
		
		sPhotos = Ti.UI.createScrollView({
			top: 5,
			left: 300,
			contentHeight: 65,
			contentWidth: 'auto',
			showHorizontalScrollIndicator: false,
			showVerticalScrollIndicator: false
		});
		
		/*
		 * Add avatar photos
		 */
		var aObj;
		for(var avatars in avatarArray) {
			if (avatarArray[avatars].RoomID == rObj.RoomID) {
				aObj = avatarArray[avatars].Avatars;
			}
		}	
		
		var imgView, imgViewHolder, left = 0; 
		for(var avatar in aObj) {
			if (aObj[avatar].AvatarURL != '') {
				imgViewHolder = Ti.UI.createView({
					top: 0,
					left: left,
					width: 65,
					height: 65
				});
				
				imgView = Ti.UI.createImageView({
					image: aObj[avatar].AvatarURL,
					fileID: aObj[avatar].FileID,
					borderRadius: 8,
					width: 65,
					height: 65,
					id: 'avatar'
				});
				left = parseInt(left+77);
				imgViewHolder.add(imgView);
				sPhotos.add(imgViewHolder);
			}	
		}	
		
		row.add(lTitle,lRoomStats,sPhotos);
		rData.push(row);	
	}	
	tableRoomsView.setData(rData);
	
	tableRoomsView.addEventListener('click', function(e) {
		var propertyID = e.rowData.propertyID;
		var roomID     = e.rowData.roomID;
		
		if (e.source.id == 'avatar') {
			var imgObj = {
				url: e.source.image,
				fileID: e.source.fileID
			}
			InventoryModalWindow.init(imgObj,'fileView');
			InventoryModalWindow.win.open({
				modal: true,
				modalStyle: Titanium.UI.iPhone.MODAL_PRESENTATION_FORMSHEET
			});
		} else {
			AppInit.detailWindow.title = e.rowData.propertyName+' : '+e.rowData.roomName;
			AppInit.splitView.setMasterPopupVisible(false);
			InventoryView.init(propertyID,roomID);	
		}
	});
	
	Property.view.add(Property.renderPropertyRow(pObj,true),tableRoomsView);
	AppInit.detailWindow.animate({view: Property.view, transition: Ti.UI.iOS.ANIMATION_CURVE_EASE_IN});		
}

Property.renderPropertyRow = function(pObj,panel) {
	if (panel == true) {
		var rowDashboard = Ti.UI.createView({
			width: 'auto',
			height: 120,
			backgroundColor: '#fff',
			borderColor: '#ccc',
			top: 0,
			left: 0
		});
	} else {
		var rowDashboard = Ti.UI.createTableViewRow({
			height: 110, 
			hasChild: true, 
			rooms: pObj.Rooms, 
			propertyID: pObj.PlaceID,
			propertyName: pObj.PlaceName
		});
	}	
	
	var lPTitleDashboard  = Ti.UI.createLabel({
		text: pObj.PlaceName, 
		top: 8, 
		left: 70, 
		width: 240,
		height: 20,
		font: {fontSize: 18, fontWeight: 'bold'}
	});
	
	var lCreatedDashboard = Ti.UI.createLabel({
		text: 'Created by '+pObj.EnteredBy+' on '+pObj.DateEnteredHuman,
		top: 28,
		left: 70,
		width: 240,
		height: 20,
		font: { fontSize: 12 },
		color: '#464c4d'
	});
	
	var lAddressDashboard = Ti.UI.createLabel({
		text: pObj.Address+'\n'+pObj.City+' '+pObj.State+' '+pObj.Zip,
		top: 57,
		left: 70,
		width: 260,
		height: 50
	});
			
	var lPropertyDetails = Ti.UI.createLabel({
		top: 59,
		left: 300,
		width: 200,
		height: 40,
		text: pObj.Category+', '+pObj.Area+' square '+pObj.AreaUnit
	});
	
	var lAvatarDashboard = Ti.UI.createLabel({
		backgroundImage: Properties.getPropertyAvatar(pObj.AvatarClass),
		width: 48,
		height: 48,
		left: 8,
		top: 10
	});
	
	var lMap = Ti.UI.createLabel({
		backgroundImage: 'images/map.png',
		width: 32,
		height: 32,
		top: 68,
		left: 16,
		id: 'map',
		pObj: pObj
	});
			
	var lTotalValueDashboard = Ti.UI.createLabel({
	    text: pObj.TotalPriceReplaceRender,
	    font: { 
	    	fontSize: 24,
	    	fontWeight: 'bold',
	    	fontFamily: 'franklin'
	    },
	    textAlign: 'center',
	    color: '#265a48',
	    top: 30,
	    right: 50,
	    width: 150,
	    height: 50,
	    borderRadius: 8,
	    borderColor: '#ccc',
	    shadowColor: '#fff',
	    shadowOffset:{ x:1, y:1},
	    backgroundGradient: Styles.gradient('ltGray')
	});
	rowDashboard.add(
		lPTitleDashboard,
		lAvatarDashboard,
		lTotalValueDashboard,
		lCreatedDashboard,
		lAddressDashboard,
		lPropertyDetails,
		lMap
	);
	
	if (panel == true) {
		lMap.addEventListener('click', function(e) {
			MapModalWindow.init(pObj);
			MapModalWindow.win.open({
				modal: true,
				modalStyle: Titanium.UI.iPhone.MODAL_PRESENTATION_FORMSHEET
			});
			
		});
	}
	
	return rowDashboard;
}
