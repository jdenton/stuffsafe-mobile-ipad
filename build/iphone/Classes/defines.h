// Warning: this is generated file. Do not modify!

#define TI_VERSION 2.0.1.GA2
#define USE_TI_ANALYTICS
#define USE_TI_NETWORK
#define USE_TI_PLATFORM
#define USE_TI_UI
#define USE_TI_API
#define USE_TI_INCLUDE
#define USE_TI_UIWINDOW
#define USE_TI_UIIPHONE
#define USE_TI_UIIPHONENAVIGATIONGROUP
#define USE_TI_UIIPAD
#define USE_TI_UIIPADSPLITWINDOW
#define USE_TI_UITABLEVIEW
#define USE_TI_UITABLEVIEWROW
#define USE_TI_UILABEL
#define USE_TI_UIIOS
#define USE_TI_UIIOSANIMATION_CURVE_EASE_IN
#define USE_TI_UIBUTTONBAR
#define USE_TI_UIIPHONESYSTEMBUTTONSTYLE
#define USE_TI_UIIPHONESYSTEMBUTTONSTYLEBAR
#define USE_TI_UIIPHONEMODAL_PRESENTATION_FORMSHEET
#define USE_TI_UIBUTTON
#define USE_TI_UIIPHONESYSTEMBUTTONSTYLEBORDERED
#define USE_TI_APIINFO
#define USE_TI_DATABASE
#define USE_TI_DATABASEOPEN
#define USE_TI_NETWORKHTTPCLIENT
#define USE_TI_APP
#define USE_TI_APPFIREEVENT
#define USE_TI_UIIPHONEAPPBADGE
#define USE_TI_APPPROPERTIES
#define USE_TI_APPPROPERTIESSETSTRING
#define USE_TI_PLATFORMOSNAME
#define USE_TI_UIACTIVITYINDICATOR
#define USE_TI_UIVIEW
#define USE_TI_UIIPHONEACTIVITYINDICATORSTYLE
#define USE_TI_UIIPHONEACTIVITYINDICATORSTYLEBIG
#define USE_TI_UIALERTDIALOG
#define USE_TI_UIWEBVIEW
#define USE_TI_UISCROLLABLEVIEW
#define USE_TI_UISCROLLVIEW
#define USE_TI_UIIMAGEVIEW
#define USE_TI_FILESYSTEM
#define USE_TI_FILESYSTEMGETFILE
#define USE_TI_FILESYSTEMAPPLICATIONDATADIRECTORY
#define USE_TI_UIIPADDOCUMENTVIEWER
#define USE_TI_UIIPHONESYSTEMBUTTON
#define USE_TI_UIIPHONESYSTEMBUTTONFLEXIBLE_SPACE
#define USE_TI_UIIPHONESYSTEMBUTTONTRASH
#define USE_TI_UIOPTIONDIALOG
#define USE_TI_UIIPHONESYSTEMBUTTONSTYLEDONE
#define USE_TI_UIIPADPOPOVER
#define USE_TI_UIIPADPOPOVER_ARROW_DIRECTION_LEFT
#define USE_TI_UIPICKER
#define USE_TI_UIPICKER_TYPE_DATE
#define USE_TI_UIIPHONETABLEVIEWSTYLE
#define USE_TI_UIIPHONETABLEVIEWSTYLEGROUPED
#define USE_TI_UITEXTFIELD
#define USE_TI_UIINPUT_BUTTONMODE_ONFOCUS
#define USE_TI_UIKEYBOARD_DEFAULT
#define USE_TI_UIRETURNKEY_DONE
#define USE_TI_UITEXTAREA
#define USE_TI_UIKEYBOARD_NUMBER_PAD
#define USE_TI_MEDIA
#define USE_TI_MEDIASHOWCAMERA
#define USE_TI_MEDIANO_CAMERA
#define USE_TI_MEDIAMEDIA_TYPE_VIDEO
#define USE_TI_MEDIAMEDIA_TYPE_PHOTO
#define USE_TI_MEDIAOPENPHOTOGALLERY
#define USE_TI_APIERROR
#define USE_TI_UIIPADPOPOVER_ARROW_DIRECTION_UP
#define USE_TI_MEDIAVIDEOPLAYER
#define USE_TI_MEDIAVIDEO_CONTROL_DEFAULT
#define USE_TI_MEDIAVIDEO_SCALING_ASPECT_FIT
#define USE_TI_UIKEYBOARD_EMAIL
#define USE_TI_UIINPUT_BORDERSTYLE_ROUNDED
#define USE_TI_UIRETURNKEY_NEXT
#define USE_TI_MAP
#define USE_TI_MAPVIEW
#define USE_TI_MAPSTANDARD_TYPE
#define USE_TI_GEOLOCATION
#define USE_TI_GEOLOCATIONFORWARDGEOCODER
#define USE_TI_MAPANNOTATION
#define USE_TI_MAPANNOTATION_RED
